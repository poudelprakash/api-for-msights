class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :update, :destroy]

  # GET /tickets
  def index
    if params[:sortBy] && params[:sortOrder]
      @tickets =  Ticket.order("#{params[:sortBy]} #{params[:sortOrder]}")
    else
      @tickets = Ticket.all
    end
    query = params[:search]
    if query
      @tickets = @tickets.where("owner LIKE ?", "%#{query}%")
    end
    if params[:page]
      @tickets = @tickets.page(params[:page][:number]).per(params[:page][:size])
    end

    render json: @tickets
  end

  # GET /tickets/1
  def show
    render json: @ticket
  end

  # POST /tickets
  def create
    @ticket = Ticket.new(ticket_params)

    if @ticket.save
      render json: @ticket, status: :created, location: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tickets/1
  def update
    if @ticket.update(ticket_params)
      render json: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tickets/1
  def destroy
    @ticket.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_ticket
    @ticket = Ticket.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def ticket_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params)
  end
end
class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :update, :destroy]

  # GET /tickets
  def index
    # binding.pry
    # Ti
    if params[:sortBy] && params[:sortOrder]
      @tickets =  Ticket.order("#{params[:sortBy]} #{params[:sortOrder]}")
    else
      @tickets = Ticket.all
    end
    query = params[:search]
    if query
      @tickets = @tickets.where("owner LIKE ?", "%#{query}%")
    end
    if params[:page]
      @tickets = @tickets.page(params[:page][:number]).per(params[:page][:size])
    end

    render json: @tickets
  end

  # GET /tickets/1
  def show
    render json: @ticket
  end

  # POST /tickets
  def create
    @ticket = Ticket.new(ticket_params)

    if @ticket.save
      render json: @ticket, status: :created, location: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tickets/1
  def update
    if @ticket.update(ticket_params)
      render json: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tickets/1
  def destroy
    @ticket.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_ticket
    @ticket = Ticket.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def ticket_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params)
  end
end
