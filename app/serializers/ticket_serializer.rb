class TicketSerializer < ActiveModel::Serializer
  attributes :id, :created, :owner, :priority, :assigned, :subject, :next_step, :my_type, :attention, :target_close
end
