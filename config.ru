# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'

class RootSiteAuth < Rack::Auth::Basic
  def call(env)
    request = Rack::Request.new(env)
    if ['/'].include? request.path
      super
    else
      @app.call(env)
    end
  end
end

use RootSiteAuth, "Restricted Area" do |username, password|
  [username, password] == ['admin', 'I@mL0ck3d']
end

run Rails.application
