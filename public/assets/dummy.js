/* jshint ignore:start */

/* jshint ignore:end */

define('dummy/app', ['exports', 'ember', 'ember/resolver', 'ember/load-initializers', 'dummy/config/environment'], function (exports, Ember, Resolver, loadInitializers, config) {

  'use strict';

  var App;

  Ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = Ember['default'].Application.extend({
    modulePrefix: config['default'].modulePrefix,
    podModulePrefix: config['default'].podModulePrefix,
    Resolver: Resolver['default']
  });

  Ember['default'].deprecate = function () {};
  loadInitializers['default'](App, config['default'].modulePrefix);

  exports['default'] = App;

});
define('dummy/components/ember-collection', ['exports', 'ember-collection/components/ember-collection'], function (exports, ember_collection) {

	'use strict';



	exports.default = ember_collection.default;

});
define('dummy/components/ember-native-scrollable', ['exports', 'ember-collection/components/ember-native-scrollable'], function (exports, ember_native_scrollable) {

	'use strict';



	exports.default = ember_native_scrollable.default;

});
define('dummy/components/fa-icon', ['exports', 'ember-font-awesome/components/fa-icon'], function (exports, fa_icon) {

	'use strict';



	exports.default = fa_icon.default;

});
define('dummy/components/fa-list', ['exports', 'ember-font-awesome/components/fa-list'], function (exports, fa_list) {

	'use strict';



	exports.default = fa_list.default;

});
define('dummy/components/fa-stack', ['exports', 'ember-font-awesome/components/fa-stack'], function (exports, fa_stack) {

	'use strict';



	exports.default = fa_stack.default;

});
define('dummy/components/lf-grid', ['exports', 'msigrid/components/lf-grid'], function (exports, lf_grid) {

	'use strict';



	exports.default = lf_grid.default;

});
define('dummy/components/msights-collection', ['exports', 'msigrid/components/msights-collection'], function (exports, msights_collection) {

	'use strict';



	exports.default = msights_collection.default;

});
define('dummy/components/msights-column-filter', ['exports', 'msigrid/components/msights-column-filter'], function (exports, msights_column_filter) {

	'use strict';



	exports.default = msights_column_filter.default;

});
define('dummy/components/msights-filter-datetime-input', ['exports', 'msigrid/components/msights-filter-datetime-input'], function (exports, msights_filter_datetime_input) {

	'use strict';



	exports.default = msights_filter_datetime_input.default;

});
define('dummy/components/msights-filter-row', ['exports', 'msigrid/components/msights-filter-row'], function (exports, msights_filter_row) {

	'use strict';



	exports.default = msights_filter_row.default;

});
define('dummy/components/msights-filter', ['exports', 'msigrid/components/msights-filter'], function (exports, msights_filter) {

	'use strict';



	exports.default = msights_filter.default;

});
define('dummy/components/msights-grid-data', ['exports', 'msigrid/components/msights-grid-data'], function (exports, msights_grid_data) {

	'use strict';



	exports.default = msights_grid_data.default;

});
define('dummy/components/msights-grid-edit-header-field', ['exports', 'msigrid/components/msights-grid-edit-header-field'], function (exports, msights_grid_edit_header_field) {

	'use strict';



	exports.default = msights_grid_edit_header_field.default;

});
define('dummy/components/msights-grid-edit-header', ['exports', 'msigrid/components/msights-grid-edit-header'], function (exports, msights_grid_edit_header) {

	'use strict';



	exports.default = msights_grid_edit_header.default;

});
define('dummy/components/msights-grid-field-date', ['exports', 'msigrid/components/msights-grid-field-date'], function (exports, msights_grid_field_date) {

	'use strict';



	exports.default = msights_grid_field_date.default;

});
define('dummy/components/msights-grid-field-datetime', ['exports', 'msigrid/components/msights-grid-field-datetime'], function (exports, msights_grid_field_datetime) {

	'use strict';



	exports.default = msights_grid_field_datetime.default;

});
define('dummy/components/msights-grid-field-decimal', ['exports', 'msigrid/components/msights-grid-field-decimal'], function (exports, msights_grid_field_decimal) {

	'use strict';



	exports.default = msights_grid_field_decimal.default;

});
define('dummy/components/msights-grid-field-dropdown', ['exports', 'msigrid/components/msights-grid-field-dropdown'], function (exports, msights_grid_field_dropdown) {

	'use strict';



	exports.default = msights_grid_field_dropdown.default;

});
define('dummy/components/msights-grid-field-integer', ['exports', 'msigrid/components/msights-grid-field-integer'], function (exports, msights_grid_field_integer) {

	'use strict';



	exports.default = msights_grid_field_integer.default;

});
define('dummy/components/msights-grid-field-string', ['exports', 'msigrid/components/msights-grid-field-string'], function (exports, msights_grid_field_string) {

	'use strict';



	exports.default = msights_grid_field_string.default;

});
define('dummy/components/msights-grid-header-data', ['exports', 'msigrid/components/msights-grid-header-data'], function (exports, msights_grid_header_data) {

	'use strict';



	exports.default = msights_grid_header_data.default;

});
define('dummy/components/msights-grid-row', ['exports', 'msigrid/components/msights-grid-row'], function (exports, msights_grid_row) {

	'use strict';



	exports.default = msights_grid_row.default;

});
define('dummy/components/msights-grid', ['exports', 'msigrid/components/msights-grid'], function (exports, msights_grid) {

	'use strict';



	exports.default = msights_grid.default;

});
define('dummy/components/msights-tags-input', ['exports', 'msigrid/components/msights-tags-input'], function (exports, msights_tags_input) {

	'use strict';



	exports.default = msights_tags_input.default;

});
define('dummy/components/msights-template-options', ['exports', 'msigrid/components/msights-template-options'], function (exports, msights_template_options) {

	'use strict';



	exports.default = msights_template_options.default;

});
define('dummy/components/msights-template-select', ['exports', 'msigrid/components/msights-template-select'], function (exports, msights_template_select) {

	'use strict';



	exports.default = msights_template_select.default;

});
define('dummy/components/multiselect-checkboxes', ['exports', 'ember-multiselect-checkboxes/components/multiselect-checkboxes'], function (exports, MultiselectCheckboxesComponent) {

	'use strict';

	exports['default'] = MultiselectCheckboxesComponent['default'];

});
define('dummy/controllers/array', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Controller;

});
define('dummy/controllers/object', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Controller;

});
define('dummy/controllers/tickets', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Controller.extend({
    config: {
      name: 'Tickets',
      toolsEnabled: true,
      csv: {
        header: {
          boardName: null, //pass null for empty
          boardDate: '25.06.20169:00 pm' //pass null for empty
        },
        footer: {
          lastResultsDate: '25.06.20169:00 pm', //pass null for empty
          generatedByMessage: "Generated by test user.",
          clientConfidential: "This document contains Confidential Information.",
          source: 'Source: MSIGHTS Platform | Copyright (c) ' + new Date().getFullYear() + ' MSIGHTS Inc. All right reserved.'
        }

      },
      format: {
        number: {
          decimalNotation: 'comma', // possible values: {dot, comma}
          precision: 3,
          alignRight: true
        },
        dateFormat: 'DD.MM.YYYY', // momentJs date formats
        timeFormat: 'h:mm a' // momentJs time formats
      },
      rowCount: 10,
      width: 1170,
      defaultGridCellWidth: 120,
      defaultTemplateId: 2,
      pagination: { //SETTING TO NULL WILL DISABLE PAGINATION
        perPage: 50,
        pages: [5, 10, 20, 50, 100],
        includeAll: true
      },
      headers: {
        "Project-Id": "16",
        "Authorization": "hl2j58rthjoq99oce0ojoav5mgfb3f4uoe69ar65egg38lh6oadu"
      },
      urls: {
        fields: {
          index: 'http://media-planner.msights-dev.com:80/data_services/api/fields.json',
          detail: 'http://media-planner.msights-dev.com:80/data_services/api/fields/:id.json'
        },
        templates: {
          index: 'http://data-services.msights-dev.com/data_services/api/templates.json',
          create: 'http://data-services.msights-dev.com/data_services/api/templates.json',
          detail: 'http://data-services.msights-dev.com/data_services/api/templates/:template_id.json?limited=true',
          update: 'http://data-services.msights-dev.com/data_services/api/templates/:template_id.json',
          destroy: 'http://data-services.msights-dev.com/data_services/api/templates/:template_id.json'
        },
        grids: {
          detail: 'http://data-services.msights-dev.com/data_services/api/grids/:id.json',
          update: 'http://data-services.msights-dev.com/data_services/api/grids/:id.json',
          create: 'http://data-services.msights-dev.com/data_services/api/grids.json'
        },
        data: {
          index: 'http://media-planner.msights-dev.com/media_planner/api/data_templates.json?template_id=:template_id&media_plan_ids=1,2,3&media_flight_ids=1,2,3',
          update: 'http://media-planner.msights-dev.com/media_planner/api/data_templates.json'
        }
      }
    },
    config2: {
      name: 'Component 2',
      toolsEnabled: true,
      format: {
        number: {
          decimalNotation: 'comma', // possible values: {dot, comma}
          precision: 3,
          alignRight: true
        },
        dateFormat: 'DD.MM.YYYY', // momentJs date formats
        timeFormat: 'h:mm a' // momentJs time formats
      },
      rowCount: 10,
      width: 1170,
      defaultGridCellWidth: 120,
      defaultTemplateId: 2,
      pagination: { //SETTING TO NULL WILL DISABLE PAGINATION
        perPage: 5,
        pages: [5, 10, 20, 50, 100]
      },
      headers: {
        "Project-Id": "16",
        "Authorization": "hl2j58rthjoq99oce0ojoav5mgfb3f4uoe69ar65egg38lh6oadu"
      },
      urls: {
        fields: {
          index: 'http://media-planner.msights-dev.com:80/data_services/api/fields.json',
          detail: 'http://media-planner.msights-dev.com:80/data_services/api/fields/:id.json'
        },
        templates: {
          index: 'http://data-services.msights-dev.com/data_services/api/templates.json',
          create: 'http://data-services.msights-dev.com/data_services/api/templates.json',
          detail: 'http://data-services.msights-dev.com/data_services/api/templates/:template_id.json?limited=true',
          update: 'http://data-services.msights-dev.com/data_services/api/templates/:template_id.json',
          destroy: 'http://data-services.msights-dev.com/data_services/api/templates/:template_id.json'
        },
        grids: {
          detail: 'http://data-services.msights-dev.com/data_services/api/grids/:id.json',
          update: 'http://data-services.msights-dev.com/data_services/api/grids/:id.json',
          create: 'http://data-services.msights-dev.com/data_services/api/grids.json'
        },
        data: {
          index: 'http://media-planner.msights-dev.com/media_planner/api/data_templates.json?template_id=:template_id&media_plan_ids=1,2,3&media_flight_ids=1,2,3',
          update: 'http://media-planner.msights-dev.com/media_planner/api/data_templates.json'
        }
      }
    }
  });

});
define('dummy/helpers/equals', ['exports', 'msigrid/helpers/equals'], function (exports, equals) {

	'use strict';



	exports.default = equals.default;
	exports.equals = equals.equals;

});
define('dummy/helpers/fixed-grid-layout', ['exports', 'ember', 'ember-collection/layouts/grid'], function (exports, Ember, Grid) {

  'use strict';

  exports['default'] = Ember['default'].Helper.helper(function (params, hash) {
    return new Grid['default'](params[0], params[1]);
  });

});
define('dummy/helpers/format-money', ['exports', 'accounting/helpers/format-money'], function (exports, Helper) {

	'use strict';

	exports['default'] = Helper['default'];

});
define('dummy/helpers/format-number', ['exports', 'accounting/helpers/format-number'], function (exports, Helper) {

	'use strict';

	exports['default'] = Helper['default'];

});
define('dummy/helpers/mixed-grid-layout', ['exports', 'ember', 'ember-collection/layouts/mixed-grid'], function (exports, Ember, MixedGrid) {

  'use strict';

  exports['default'] = Ember['default'].Helper.helper(function (params, hash) {
    return new MixedGrid['default'](params[0]);
  });

});
define('dummy/helpers/msights-grid-data-value', ['exports', 'msigrid/helpers/msights-grid-data-value'], function (exports, msights_grid_data_value) {

	'use strict';



	exports.default = msights_grid_data_value.default;
	exports.msightsGridDataValue = msights_grid_data_value.msightsGridDataValue;

});
define('dummy/helpers/msights-object-value', ['exports', 'msigrid/helpers/msights-object-value'], function (exports, msights_object_value) {

	'use strict';



	exports.default = msights_object_value.default;
	exports.msightsObjectValue = msights_object_value.msightsObjectValue;

});
define('dummy/helpers/pagination-range', ['exports', 'msigrid/helpers/pagination-range'], function (exports, pagination_range) {

	'use strict';



	exports.default = pagination_range.default;
	exports.paginationRange = pagination_range.paginationRange;

});
define('dummy/helpers/percentage-columns-layout', ['exports', 'ember', 'ember-collection/layouts/percentage-columns'], function (exports, Ember, PercentageColumns) {

  'use strict';

  exports['default'] = Ember['default'].Helper.helper(function (params, hash) {
    return new PercentageColumns['default'](params[0], params[1], params[2]);
  });

});
define('dummy/helpers/proxy-page', ['exports', 'msigrid/helpers/proxy-page'], function (exports, proxy_page) {

	'use strict';



	exports.default = proxy_page.default;
	exports.proxyPage = proxy_page.proxyPage;

});
define('dummy/initializers/export-application-global', ['exports', 'ember', 'dummy/config/environment'], function (exports, Ember, config) {

  'use strict';

  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (config['default'].exportApplicationGlobal !== false) {
      var value = config['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember['default'].String.classify(config['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };

});
define('dummy/instance-initializers/app-version', ['exports', 'dummy/config/environment', 'ember'], function (exports, config, Ember) {

  'use strict';

  var classify = Ember['default'].String.classify;
  var registered = false;

  exports['default'] = {
    name: 'App Version',
    initialize: function initialize(application) {
      if (!registered) {
        var appName = classify(application.toString());
        Ember['default'].libraries.register(appName, config['default'].APP.version);
        registered = true;
      }
    }
  };

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-collection.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-collection.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-collection.js should pass jshint.\nmodules/msigrid/components/msights-collection.js: line 124, col 19, \'i\' is already defined.\nmodules/msigrid/components/msights-collection.js: line 125, col 17, \'bufferRow\' is already defined.\nmodules/msigrid/components/msights-collection.js: line 98, col 13, \'newArray\' is defined but never used.\nmodules/msigrid/components/msights-collection.js: line 72, col 33, \'event\' is defined but never used.\n\n4 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-column-filter.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-column-filter.js should pass jshint', function () {
    ok(true, 'modules/msigrid/components/msights-column-filter.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-filter-datetime-input.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-filter-datetime-input.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-filter-datetime-input.js should pass jshint.\nmodules/msigrid/components/msights-filter-datetime-input.js: line 25, col 52, Unnecessary semicolon.\nmodules/msigrid/components/msights-filter-datetime-input.js: line 10, col 26, \'$\' is not defined.\nmodules/msigrid/components/msights-filter-datetime-input.js: line 21, col 31, \'moment\' is not defined.\n\n3 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-filter-row.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-filter-row.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-filter-row.js should pass jshint.\nmodules/msigrid/components/msights-filter-row.js: line 29, col 9, \'self\' is defined but never used.\nmodules/msigrid/components/msights-filter-row.js: line 85, col 41, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 87, col 50, Expected \'===\' and instead saw \'==\'.\nmodules/msigrid/components/msights-filter-row.js: line 88, col 25, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 95, col 51, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 102, col 48, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 104, col 28, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 106, col 71, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 109, col 74, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 112, col 75, Missing semicolon.\nmodules/msigrid/components/msights-filter-row.js: line 115, col 50, Missing semicolon.\n\n11 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-filter.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-filter.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-filter.js should pass jshint.\nmodules/msigrid/components/msights-filter.js: line 20, col 13, \'remainingFilter\' is defined but never used.\nmodules/msigrid/components/msights-filter.js: line 46, col 47, Missing semicolon.\nmodules/msigrid/components/msights-filter.js: line 45, col 11, \'invalidFilters\' is defined but never used.\nmodules/msigrid/components/msights-filter.js: line 39, col 26, \'el\' is defined but never used.\nmodules/msigrid/components/msights-filter.js: line 56, col 33, Missing semicolon.\n\n5 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-data.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-data.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid-data.js should pass jshint.\nmodules/msigrid/components/msights-grid-data.js: line 57, col 27, \'response\' is defined but never used.\nmodules/msigrid/components/msights-grid-data.js: line 83, col 5, \'$\' is not defined.\n\n2 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-edit-header-field.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-edit-header-field.js should pass jshint', function () {
    ok(true, 'modules/msigrid/components/msights-grid-edit-header-field.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-edit-header.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-edit-header.js should pass jshint', function () {
    ok(true, 'modules/msigrid/components/msights-grid-edit-header.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-field-date.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-field-date.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid-field-date.js should pass jshint.\nmodules/msigrid/components/msights-grid-field-date.js: line 24, col 52, Unnecessary semicolon.\nmodules/msigrid/components/msights-grid-field-date.js: line 10, col 22, \'$\' is not defined.\nmodules/msigrid/components/msights-grid-field-date.js: line 20, col 31, \'moment\' is not defined.\n\n3 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-field-datetime.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-field-datetime.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid-field-datetime.js should pass jshint.\nmodules/msigrid/components/msights-grid-field-datetime.js: line 31, col 52, Unnecessary semicolon.\nmodules/msigrid/components/msights-grid-field-datetime.js: line 13, col 26, \'$\' is not defined.\nmodules/msigrid/components/msights-grid-field-datetime.js: line 24, col 31, \'moment\' is not defined.\n\n3 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-field-decimal.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-field-decimal.js should pass jshint', function () {
    ok(true, 'modules/msigrid/components/msights-grid-field-decimal.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-field-dropdown.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-field-dropdown.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid-field-dropdown.js should pass jshint.\nmodules/msigrid/components/msights-grid-field-dropdown.js: line 32, col 26, Expected \'!==\' and instead saw \'!=\'.\nmodules/msigrid/components/msights-grid-field-dropdown.js: line 45, col 77, Missing semicolon.\nmodules/msigrid/components/msights-grid-field-dropdown.js: line 68, col 6, Unnecessary semicolon.\nmodules/msigrid/components/msights-grid-field-dropdown.js: line 77, col 85, Missing semicolon.\nmodules/msigrid/components/msights-grid-field-dropdown.js: line 80, col 83, Missing semicolon.\n\n5 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-field-integer.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-field-integer.js should pass jshint', function () {
    ok(true, 'modules/msigrid/components/msights-grid-field-integer.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-field-string.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-field-string.js should pass jshint', function () {
    ok(true, 'modules/msigrid/components/msights-grid-field-string.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-header-data.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-header-data.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid-header-data.js should pass jshint.\nmodules/msigrid/components/msights-grid-header-data.js: line 17, col 9, \'self\' is defined but never used.\nmodules/msigrid/components/msights-grid-header-data.js: line 64, col 29, \'ui\' is defined but never used.\nmodules/msigrid/components/msights-grid-header-data.js: line 64, col 22, \'event\' is defined but never used.\n\n3 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid-row.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid-row.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid-row.js should pass jshint.\nmodules/msigrid/components/msights-grid-row.js: line 57, col 53, Missing semicolon.\nmodules/msigrid/components/msights-grid-row.js: line 66, col 54, Missing semicolon.\n\n2 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-grid.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-grid.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-grid.js should pass jshint.\nmodules/msigrid/components/msights-grid.js: line 58, col 33, \'validationObject\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 95, col 16, \'error\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 225, col 9, \'self\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 280, col 15, \'i\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 300, col 30, \'error\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 300, col 21, Don\'t make functions within a loop.\nmodules/msigrid/components/msights-grid.js: line 326, col 9, \'gridConfig\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 361, col 7, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 373, col 33, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 379, col 109, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 409, col 38, Use the isNaN function to compare with NaN.\nmodules/msigrid/components/msights-grid.js: line 409, col 47, Expected \'{\' and instead saw \'payload\'.\nmodules/msigrid/components/msights-grid.js: line 536, col 39, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 544, col 42, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 551, col 13, \'value1\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 552, col 13, \'value2\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 553, col 28, Expected \'{\' and instead saw \'return\'.\nmodules/msigrid/components/msights-grid.js: line 554, col 28, Expected \'{\' and instead saw \'return\'.\nmodules/msigrid/components/msights-grid.js: line 562, col 13, \'value1\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 563, col 13, \'value2\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 564, col 28, Expected \'{\' and instead saw \'return\'.\nmodules/msigrid/components/msights-grid.js: line 565, col 28, Expected \'{\' and instead saw \'return\'.\nmodules/msigrid/components/msights-grid.js: line 541, col 9, \'column\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 630, col 64, \'index\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 672, col 13, \'i\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 674, col 15, \'j\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 675, col 13, \'field\' is already defined.\nmodules/msigrid/components/msights-grid.js: line 724, col 22, Use the isNaN function to compare with NaN.\nmodules/msigrid/components/msights-grid.js: line 724, col 31, Expected \'{\' and instead saw \'value\'.\nmodules/msigrid/components/msights-grid.js: line 715, col 15, \'dataId\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 740, col 55, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 814, col 21, Expected \'{\' and instead saw \'templates\'.\nmodules/msigrid/components/msights-grid.js: line 822, col 62, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 833, col 70, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 830, col 9, \'self\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 883, col 30, Expected \'===\' and instead saw \'==\'.\nmodules/msigrid/components/msights-grid.js: line 887, col 44, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 904, col 28, \'callback\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 915, col 50, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 917, col 36, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 967, col 41, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 992, col 32, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 1097, col 31, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 1148, col 20, Use the isNaN function to compare with NaN.\nmodules/msigrid/components/msights-grid.js: line 1148, col 29, Expected \'{\' and instead saw \'value\'.\nmodules/msigrid/components/msights-grid.js: line 1215, col 71, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 1205, col 11, \'fields\' is defined but never used.\nmodules/msigrid/components/msights-grid.js: line 1221, col 37, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 1244, col 83, Missing semicolon.\nmodules/msigrid/components/msights-grid.js: line 1246, col 33, Expected \'===\' and instead saw \'==\'.\nmodules/msigrid/components/msights-grid.js: line 1246, col 33, Too many errors. (86% scanned).\n\n52 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-tags-input.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-tags-input.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-tags-input.js should pass jshint.\nmodules/msigrid/components/msights-tags-input.js: line 44, col 88, Missing semicolon.\nmodules/msigrid/components/msights-tags-input.js: line 47, col 84, Missing semicolon.\n\n2 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-template-options.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-template-options.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-template-options.js should pass jshint.\nmodules/msigrid/components/msights-template-options.js: line 60, col 48, Unnecessary semicolon.\n\n1 error');
  });

});
define('dummy/msigrid/tests/modules/msigrid/components/msights-template-select.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/components');
  test('modules/msigrid/components/msights-template-select.js should pass jshint', function () {
    ok(false, 'modules/msigrid/components/msights-template-select.js should pass jshint.\nmodules/msigrid/components/msights-template-select.js: line 38, col 70, Missing semicolon.\n\n1 error');
  });

});
define('dummy/msigrid/tests/modules/msigrid/helpers/equals.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/helpers');
  test('modules/msigrid/helpers/equals.js should pass jshint', function () {
    ok(true, 'modules/msigrid/helpers/equals.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/helpers/msights-grid-data-value.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/helpers');
  test('modules/msigrid/helpers/msights-grid-data-value.js should pass jshint', function () {
    ok(true, 'modules/msigrid/helpers/msights-grid-data-value.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/helpers/msights-object-value.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/helpers');
  test('modules/msigrid/helpers/msights-object-value.js should pass jshint', function () {
    ok(true, 'modules/msigrid/helpers/msights-object-value.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/helpers/pagination-range.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/helpers');
  test('modules/msigrid/helpers/pagination-range.js should pass jshint', function () {
    ok(false, 'modules/msigrid/helpers/pagination-range.js should pass jshint.\nmodules/msigrid/helpers/pagination-range.js: line 13, col 48, Missing semicolon.\n\n1 error');
  });

});
define('dummy/msigrid/tests/modules/msigrid/helpers/proxy-page.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/helpers');
  test('modules/msigrid/helpers/proxy-page.js should pass jshint', function () {
    ok(true, 'modules/msigrid/helpers/proxy-page.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/format-helpers.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/format-helpers.js should pass jshint', function () {
    ok(false, 'modules/msigrid/utils/format-helpers.js should pass jshint.\nmodules/msigrid/utils/format-helpers.js: line 19, col 2, Unnecessary semicolon.\nmodules/msigrid/utils/format-helpers.js: line 24, col 2, Unnecessary semicolon.\n\n2 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/ie-helpers.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/ie-helpers.js should pass jshint', function () {
    ok(true, 'modules/msigrid/utils/ie-helpers.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/model-helpers.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/model-helpers.js should pass jshint', function () {
    ok(true, 'modules/msigrid/utils/model-helpers.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/msights-array.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/msights-array.js should pass jshint', function () {
    ok(false, 'modules/msigrid/utils/msights-array.js should pass jshint.\nmodules/msigrid/utils/msights-array.js: line 2, col 24, Missing semicolon.\nmodules/msigrid/utils/msights-array.js: line 21, col 32, Expected \'===\' and instead saw \'==\'.\nmodules/msigrid/utils/msights-array.js: line 23, col 2, Unnecessary semicolon.\n\n3 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/msights-filter.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/msights-filter.js should pass jshint', function () {
    ok(false, 'modules/msigrid/utils/msights-filter.js should pass jshint.\nmodules/msigrid/utils/msights-filter.js: line 6, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 9, col 37, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 14, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 22, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 30, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 38, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 76, col 19, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 82, col 49, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 84, col 23, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 92, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 95, col 37, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 99, col 34, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 102, col 37, Missing semicolon.\nmodules/msigrid/utils/msights-filter.js: line 3, col 21, \'Ember\' is not defined.\nmodules/msigrid/utils/msights-filter.js: line 72, col 29, \'Ember\' is not defined.\n\n15 errors');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/msights-helpers.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/msights-helpers.js should pass jshint', function () {
    ok(true, 'modules/msigrid/utils/msights-helpers.js should pass jshint.');
  });

});
define('dummy/msigrid/tests/modules/msigrid/utils/msights-search.jshint', function () {

  'use strict';

  module('JSHint - modules/msigrid/utils');
  test('modules/msigrid/utils/msights-search.js should pass jshint', function () {
    ok(false, 'modules/msigrid/utils/msights-search.js should pass jshint.\nmodules/msigrid/utils/msights-search.js: line 3, col 25, \'Ember\' is not defined.\n\n1 error');
  });

});
define('dummy/router', ['exports', 'ember', 'dummy/config/environment'], function (exports, Ember, config) {

  'use strict';

  var Router = Ember['default'].Router.extend({
    location: config['default'].locationType
  });

  Router.map(function () {
    this.route('tickets', { path: '/' });
  });

  exports['default'] = Router;

});
define('dummy/routes/grid-api', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Route.extend({});

});
define('dummy/templates/application', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      meta: {
        "revision": "Ember@1.13.3",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "dummy/templates/application.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [
        ["content","outlet",["loc",[null,[1,0],[1,10]]]]
      ],
      locals: [],
      templates: []
    };
  }()));

});
define('dummy/templates/components/multiselect-checkboxes', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    var child0 = (function() {
      var child0 = (function() {
        return {
          meta: {
            "revision": "Ember@1.13.3",
            "loc": {
              "source": null,
              "start": {
                "line": 2,
                "column": 2
              },
              "end": {
                "line": 4,
                "column": 2
              }
            },
            "moduleName": "dummy/templates/components/multiselect-checkboxes.hbs"
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment,1,1,contextualElement);
            return morphs;
          },
          statements: [
            ["inline","yield",[["get","checkbox.option",["loc",[null,[3,12],[3,27]]]],["get","checkbox.isSelected",["loc",[null,[3,28],[3,47]]]]],[],["loc",[null,[3,4],[3,49]]]]
          ],
          locals: [],
          templates: []
        };
      }());
      var child1 = (function() {
        return {
          meta: {
            "revision": "Ember@1.13.3",
            "loc": {
              "source": null,
              "start": {
                "line": 4,
                "column": 2
              },
              "end": {
                "line": 11,
                "column": 2
              }
            },
            "moduleName": "dummy/templates/components/multiselect-checkboxes.hbs"
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("li");
            var el2 = dom.createTextNode("\n      ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("label");
            var el3 = dom.createTextNode("\n        ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n        ");
            dom.appendChild(el2, el3);
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n      ");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n    ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [1, 1]);
            var morphs = new Array(2);
            morphs[0] = dom.createMorphAt(element0,1,1);
            morphs[1] = dom.createMorphAt(element0,3,3);
            return morphs;
          },
          statements: [
            ["inline","input",[],["type","checkbox","checked",["subexpr","@mut",[["get","checkbox.isSelected",["loc",[null,[7,40],[7,59]]]]],[],[]],"disabled",["subexpr","@mut",[["get","disabled",["loc",[null,[7,69],[7,77]]]]],[],[]]],["loc",[null,[7,8],[7,79]]]],
            ["content","checkbox.label",["loc",[null,[8,8],[8,26]]]]
          ],
          locals: [],
          templates: []
        };
      }());
      return {
        meta: {
          "revision": "Ember@1.13.3",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 12,
              "column": 0
            }
          },
          "moduleName": "dummy/templates/components/multiselect-checkboxes.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment,0,0,contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [
          ["block","if",[["get","hasBlock",["loc",[null,[2,8],[2,16]]]]],[],0,1,["loc",[null,[2,2],[11,9]]]]
        ],
        locals: ["checkbox"],
        templates: [child0, child1]
      };
    }());
    return {
      meta: {
        "revision": "Ember@1.13.3",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 13,
            "column": 0
          }
        },
        "moduleName": "dummy/templates/components/multiselect-checkboxes.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment,0,0,contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [
        ["block","each",[["get","checkboxes",["loc",[null,[1,8],[1,18]]]]],[],0,null,["loc",[null,[1,0],[12,9]]]]
      ],
      locals: [],
      templates: [child0]
    };
  }()));

});
define('dummy/templates/tickets', ['exports'], function (exports) {

  'use strict';

  exports['default'] = Ember.HTMLBars.template((function() {
    return {
      meta: {
        "revision": "Ember@1.13.3",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 4,
            "column": 0
          }
        },
        "moduleName": "dummy/templates/tickets.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1,"class","seperator");
        dom.setAttribute(el1,"style","height: 40px");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(2);
        morphs[0] = dom.createMorphAt(fragment,0,0,contextualElement);
        morphs[1] = dom.createMorphAt(fragment,4,4,contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [
        ["inline","msights-grid",[],["config",["subexpr","@mut",[["get","config",["loc",[null,[1,22],[1,28]]]]],[],[]]],["loc",[null,[1,0],[1,30]]]],
        ["inline","msights-grid",[],["config",["subexpr","@mut",[["get","config2",["loc",[null,[3,22],[3,29]]]]],[],[]]],["loc",[null,[3,0],[3,31]]]]
      ],
      locals: [],
      templates: []
    };
  }()));

});
define('dummy/tests/app.jshint', function () {

  'use strict';

  module('JSHint - .');
  test('app.js should pass jshint', function() { 
    ok(true, 'app.js should pass jshint.'); 
  });

});
define('dummy/tests/controllers/tickets.jshint', function () {

  'use strict';

  module('JSHint - controllers');
  test('controllers/tickets.js should pass jshint', function() { 
    ok(true, 'controllers/tickets.js should pass jshint.'); 
  });

});
define('dummy/tests/helpers/resolver', ['exports', 'ember/resolver', 'dummy/config/environment'], function (exports, Resolver, config) {

  'use strict';

  var resolver = Resolver['default'].create();

  resolver.namespace = {
    modulePrefix: config['default'].modulePrefix,
    podModulePrefix: config['default'].podModulePrefix
  };

  exports['default'] = resolver;

});
define('dummy/tests/helpers/resolver.jshint', function () {

  'use strict';

  module('JSHint - helpers');
  test('helpers/resolver.js should pass jshint', function() { 
    ok(true, 'helpers/resolver.js should pass jshint.'); 
  });

});
define('dummy/tests/helpers/start-app', ['exports', 'ember', 'dummy/app', 'dummy/config/environment'], function (exports, Ember, Application, config) {

  'use strict';



  exports['default'] = startApp;
  function startApp(attrs) {
    var application;

    var attributes = Ember['default'].merge({}, config['default'].APP);
    attributes = Ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    Ember['default'].run(function () {
      application = Application['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
    });

    return application;
  }

});
define('dummy/tests/helpers/start-app.jshint', function () {

  'use strict';

  module('JSHint - helpers');
  test('helpers/start-app.js should pass jshint', function() { 
    ok(true, 'helpers/start-app.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/lf-grid-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('lf-grid', 'Integration | Component | lf grid', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 11
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'lf-grid', ['loc', [null, [1, 0], [1, 11]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'lf-grid', [], [], 0, null, ['loc', [null, [2, 4], [4, 16]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/lf-grid-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/lf-grid-test.js should pass jshint', function() { 
    ok(true, 'integration/components/lf-grid-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-collection-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-collection', 'Integration | Component | msights collection', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 22
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-collection', ['loc', [null, [1, 0], [1, 22]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-collection', [], [], 0, null, ['loc', [null, [2, 4], [4, 27]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-collection-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-collection-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-collection-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-column-filter-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-column-filter', 'Integration | Component | msights column filter', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 25
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-column-filter', ['loc', [null, [1, 0], [1, 25]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-column-filter', [], [], 0, null, ['loc', [null, [2, 4], [4, 30]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-column-filter-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-column-filter-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-column-filter-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-filter-datetime-input-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-filter-datetime-input', 'Integration | Component | msights filter datetime input', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 33
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-filter-datetime-input', ['loc', [null, [1, 0], [1, 33]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-filter-datetime-input', [], [], 0, null, ['loc', [null, [2, 4], [4, 38]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-filter-datetime-input-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-filter-datetime-input-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-filter-datetime-input-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-filter-row-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-filter-row', 'Integration | Component | msights filter row', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 22
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-filter-row', ['loc', [null, [1, 0], [1, 22]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-filter-row', [], [], 0, null, ['loc', [null, [2, 4], [4, 27]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-filter-row-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-filter-row-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-filter-row-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-filter-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-filter', 'Integration | Component | msights filter', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 18
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-filter', ['loc', [null, [1, 0], [1, 18]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-filter', [], [], 0, null, ['loc', [null, [2, 4], [4, 23]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-filter-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-filter-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-filter-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-data-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-data', 'Integration | Component | msights grid data', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 21
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-data', ['loc', [null, [1, 0], [1, 21]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-data', [], [], 0, null, ['loc', [null, [2, 4], [4, 26]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-data-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-data-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-data-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-edit-header-field-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-edit-header-field', 'Integration | Component | msights grid edit header field', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 34
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-edit-header-field', ['loc', [null, [1, 0], [1, 34]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-edit-header-field', [], [], 0, null, ['loc', [null, [2, 4], [4, 39]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-edit-header-field-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-edit-header-field-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-edit-header-field-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-edit-header-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-edit-header', 'Integration | Component | msights grid edit header', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 28
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-edit-header', ['loc', [null, [1, 0], [1, 28]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-edit-header', [], [], 0, null, ['loc', [null, [2, 4], [4, 33]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-edit-header-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-edit-header-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-edit-header-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-field-date-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-field-date', 'Integration | Component | msights grid field date', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 27
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-field-date', ['loc', [null, [1, 0], [1, 27]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-field-date', [], [], 0, null, ['loc', [null, [2, 4], [4, 32]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-field-date-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-field-date-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-field-date-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-field-datetime-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-field-datetime', 'Integration | Component | msights grid field datetime', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 31
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-field-datetime', ['loc', [null, [1, 0], [1, 31]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-field-datetime', [], [], 0, null, ['loc', [null, [2, 4], [4, 36]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-field-datetime-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-field-datetime-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-field-datetime-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-field-decimal-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-field-decimal', 'Integration | Component | msights grid field decimal', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 30
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-field-decimal', ['loc', [null, [1, 0], [1, 30]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-field-decimal', [], [], 0, null, ['loc', [null, [2, 4], [4, 35]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-field-decimal-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-field-decimal-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-field-decimal-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-field-dropdown-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-field-dropdown', 'Integration | Component | msights grid field dropdown', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 31
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-field-dropdown', ['loc', [null, [1, 0], [1, 31]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-field-dropdown', [], [], 0, null, ['loc', [null, [2, 4], [4, 36]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-field-dropdown-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-field-dropdown-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-field-dropdown-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-field-integer-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-field-integer', 'Integration | Component | msights grid field integer', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 30
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-field-integer', ['loc', [null, [1, 0], [1, 30]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-field-integer', [], [], 0, null, ['loc', [null, [2, 4], [4, 35]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-field-integer-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-field-integer-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-field-integer-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-field-string-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-field-string', 'Integration | Component | msights grid field string', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 29
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-field-string', ['loc', [null, [1, 0], [1, 29]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-field-string', [], [], 0, null, ['loc', [null, [2, 4], [4, 34]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-field-string-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-field-string-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-field-string-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-header-data-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-header-data', 'Integration | Component | msights grid header data', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 28
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-header-data', ['loc', [null, [1, 0], [1, 28]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-header-data', [], [], 0, null, ['loc', [null, [2, 4], [4, 33]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-header-data-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-header-data-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-header-data-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-row-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid-row', 'Integration | Component | msights grid row', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 20
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid-row', ['loc', [null, [1, 0], [1, 20]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid-row', [], [], 0, null, ['loc', [null, [2, 4], [4, 25]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-row-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-row-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-row-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-grid-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-grid', 'Integration | Component | msights grid', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 16
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-grid', ['loc', [null, [1, 0], [1, 16]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-grid', [], [], 0, null, ['loc', [null, [2, 4], [4, 21]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-grid-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-grid-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-grid-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-tags-input-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-tags-input', 'Integration | Component | msights tags input', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 22
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-tags-input', ['loc', [null, [1, 0], [1, 22]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-tags-input', [], [], 0, null, ['loc', [null, [2, 4], [4, 27]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-tags-input-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-tags-input-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-tags-input-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-template-options-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-template-options', 'Integration | Component | msights template options', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 28
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-template-options', ['loc', [null, [1, 0], [1, 28]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-template-options', [], [], 0, null, ['loc', [null, [2, 4], [4, 33]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-template-options-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-template-options-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-template-options-test.js should pass jshint.'); 
  });

});
define('dummy/tests/integration/components/msights-template-select-test', ['ember-qunit'], function (ember_qunit) {

  'use strict';

  ember_qunit.moduleForComponent('msights-template-select', 'Integration | Component | msights template select', {
    integration: true
  });

  ember_qunit.test('it renders', function (assert) {
    assert.expect(2);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template((function () {
      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 1,
              'column': 27
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [['content', 'msights-template-select', ['loc', [null, [1, 0], [1, 27]]]]],
        locals: [],
        templates: []
      };
    })()));

    assert.equal(this.$().text(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template((function () {
      var child0 = (function () {
        return {
          meta: {
            'revision': 'Ember@1.13.3',
            'loc': {
              'source': null,
              'start': {
                'line': 2,
                'column': 4
              },
              'end': {
                'line': 4,
                'column': 4
              }
            }
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode('      template block text\n');
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();

      return {
        meta: {
          'revision': 'Ember@1.13.3',
          'loc': {
            'source': null,
            'start': {
              'line': 1,
              'column': 0
            },
            'end': {
              'line': 5,
              'column': 2
            }
          }
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode('\n');
          dom.appendChild(el0, el1);
          var el1 = dom.createComment('');
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode('  ');
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [['block', 'msights-template-select', [], [], 0, null, ['loc', [null, [2, 4], [4, 32]]]]],
        locals: [],
        templates: [child0]
      };
    })()));

    assert.equal(this.$().text().trim(), 'template block text');
  });

});
define('dummy/tests/integration/components/msights-template-select-test.jshint', function () {

  'use strict';

  module('JSHint - integration/components');
  test('integration/components/msights-template-select-test.js should pass jshint', function() { 
    ok(true, 'integration/components/msights-template-select-test.js should pass jshint.'); 
  });

});
define('dummy/tests/router.jshint', function () {

  'use strict';

  module('JSHint - .');
  test('router.js should pass jshint', function() { 
    ok(true, 'router.js should pass jshint.'); 
  });

});
define('dummy/tests/routes/grid-api.jshint', function () {

  'use strict';

  module('JSHint - routes');
  test('routes/grid-api.js should pass jshint', function() { 
    ok(true, 'routes/grid-api.js should pass jshint.'); 
  });

});
define('dummy/tests/test-helper', ['dummy/tests/helpers/resolver', 'ember-qunit'], function (resolver, ember_qunit) {

	'use strict';

	ember_qunit.setResolver(resolver['default']);

});
define('dummy/tests/test-helper.jshint', function () {

  'use strict';

  module('JSHint - .');
  test('test-helper.js should pass jshint', function() { 
    ok(true, 'test-helper.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/helpers/equals-test', ['dummy/helpers/equals', 'qunit'], function (equals, qunit) {

  'use strict';

  qunit.module('Unit | Helper | equals');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = equals.equals(42);
    assert.ok(result);
  });

});
define('dummy/tests/unit/helpers/equals-test.jshint', function () {

  'use strict';

  module('JSHint - unit/helpers');
  test('unit/helpers/equals-test.js should pass jshint', function() { 
    ok(true, 'unit/helpers/equals-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/helpers/msights-grid-data-value-test', ['dummy/helpers/msights-grid-data-value', 'qunit'], function (msights_grid_data_value, qunit) {

  'use strict';

  qunit.module('Unit | Helper | msights grid data value');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = msights_grid_data_value.msightsGridDataValue(42);
    assert.ok(result);
  });

});
define('dummy/tests/unit/helpers/msights-grid-data-value-test.jshint', function () {

  'use strict';

  module('JSHint - unit/helpers');
  test('unit/helpers/msights-grid-data-value-test.js should pass jshint', function() { 
    ok(true, 'unit/helpers/msights-grid-data-value-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/helpers/msights-object-value-test', ['dummy/helpers/msights-object-value', 'qunit'], function (msights_object_value, qunit) {

  'use strict';

  qunit.module('Unit | Helper | msights object value');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = msights_object_value.msightsObjectValue(42);
    assert.ok(result);
  });

});
define('dummy/tests/unit/helpers/msights-object-value-test.jshint', function () {

  'use strict';

  module('JSHint - unit/helpers');
  test('unit/helpers/msights-object-value-test.js should pass jshint', function() { 
    ok(true, 'unit/helpers/msights-object-value-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/helpers/pagination-range-test', ['dummy/helpers/pagination-range', 'qunit'], function (pagination_range, qunit) {

  'use strict';

  qunit.module('Unit | Helper | pagination range');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = pagination_range.paginationRange(42);
    assert.ok(result);
  });

});
define('dummy/tests/unit/helpers/pagination-range-test.jshint', function () {

  'use strict';

  module('JSHint - unit/helpers');
  test('unit/helpers/pagination-range-test.js should pass jshint', function() { 
    ok(true, 'unit/helpers/pagination-range-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/helpers/proxy-page-test', ['dummy/helpers/proxy-page', 'qunit'], function (proxy_page, qunit) {

  'use strict';

  qunit.module('Unit | Helper | proxy page');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = proxy_page.proxyPage(42);
    assert.ok(result);
  });

});
define('dummy/tests/unit/helpers/proxy-page-test.jshint', function () {

  'use strict';

  module('JSHint - unit/helpers');
  test('unit/helpers/proxy-page-test.js should pass jshint', function() { 
    ok(true, 'unit/helpers/proxy-page-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/utils/model-helpers-test', ['dummy/utils/model-helpers', 'qunit'], function (modelHelpers, qunit) {

  'use strict';

  qunit.module('Unit | Utility | model helpers');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = modelHelpers['default']();
    assert.ok(result);
  });

});
define('dummy/tests/unit/utils/model-helpers-test.jshint', function () {

  'use strict';

  module('JSHint - unit/utils');
  test('unit/utils/model-helpers-test.js should pass jshint', function() { 
    ok(true, 'unit/utils/model-helpers-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/utils/msights-array-test', ['dummy/utils/msights-array', 'qunit'], function (msightsArray, qunit) {

  'use strict';

  qunit.module('Unit | Utility | msights array');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = msightsArray['default']();
    assert.ok(result);
  });

});
define('dummy/tests/unit/utils/msights-array-test.jshint', function () {

  'use strict';

  module('JSHint - unit/utils');
  test('unit/utils/msights-array-test.js should pass jshint', function() { 
    ok(true, 'unit/utils/msights-array-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/utils/msights-filter-test', ['dummy/utils/msights-filter', 'qunit'], function (msightsFilter, qunit) {

  'use strict';

  qunit.module('Unit | Utility | msights filter');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = msightsFilter['default']();
    assert.ok(result);
  });

});
define('dummy/tests/unit/utils/msights-filter-test.jshint', function () {

  'use strict';

  module('JSHint - unit/utils');
  test('unit/utils/msights-filter-test.js should pass jshint', function() { 
    ok(true, 'unit/utils/msights-filter-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/utils/msights-helpers-test', ['dummy/utils/msights-helpers', 'qunit'], function (msightsHelpers, qunit) {

  'use strict';

  qunit.module('Unit | Utility | msights helpers');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = msightsHelpers['default']();
    assert.ok(result);
  });

});
define('dummy/tests/unit/utils/msights-helpers-test.jshint', function () {

  'use strict';

  module('JSHint - unit/utils');
  test('unit/utils/msights-helpers-test.js should pass jshint', function() { 
    ok(true, 'unit/utils/msights-helpers-test.js should pass jshint.'); 
  });

});
define('dummy/tests/unit/utils/msights-search-test', ['dummy/utils/msights-search', 'qunit'], function (msightsSearch, qunit) {

  'use strict';

  qunit.module('Unit | Utility | msights search');

  // Replace this with your real tests.
  qunit.test('it works', function (assert) {
    var result = msightsSearch['default']();
    assert.ok(result);
  });

});
define('dummy/tests/unit/utils/msights-search-test.jshint', function () {

  'use strict';

  module('JSHint - unit/utils');
  test('unit/utils/msights-search-test.js should pass jshint', function() { 
    ok(true, 'unit/utils/msights-search-test.js should pass jshint.'); 
  });

});
define('dummy/utils/ie-helpers', ['exports', 'grid/utils/ie-helpers'], function (exports, ie_helpers) {

	'use strict';



	exports.default = ie_helpers.default;

});
define('dummy/utils/model-helpers', ['exports', 'grid/utils/model-helpers'], function (exports, model_helpers) {

	'use strict';



	exports.default = model_helpers.default;

});
define('dummy/utils/msights-array', ['exports', 'msigrid/utils/msights-array'], function (exports, msights_array) {

	'use strict';



	exports.default = msights_array.default;

});
define('dummy/utils/msights-filter', ['exports', 'msigrid/utils/msights-filter'], function (exports, msights_filter) {

	'use strict';



	exports.default = msights_filter.default;

});
define('dummy/utils/msights-helpers', ['exports', 'msigrid/utils/msights-helpers'], function (exports, msights_helpers) {

	'use strict';



	exports.default = msights_helpers.default;

});
define('dummy/utils/msights-search', ['exports', 'msigrid/utils/msights-search'], function (exports, msights_search) {

	'use strict';



	exports.default = msights_search.default;

});
/* jshint ignore:start */

/* jshint ignore:end */

/* jshint ignore:start */

define('dummy/config/environment', ['ember'], function(Ember) {
  var prefix = 'dummy';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

if (runningTests) {
  require("dummy/tests/test-helper");
} else {
  require("dummy/app")["default"].create({"name":"msigrid","version":"0.0.1+89db2790"});
}

/* jshint ignore:end */
//# sourceMappingURL=dummy.map