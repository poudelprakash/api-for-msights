class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.datetime :created
      t.string :owner
      t.integer :priority
      t.string :assigned
      t.string :subject
      t.integer :next_step
      t.integer :my_type
      t.string :attention
      t.date :target_close

      t.timestamps
    end
  end
end
