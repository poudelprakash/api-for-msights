# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Ticket(id: integer, created: datetime, owner: string, priority: integer, assigned: string, subject: string, next_step: integer, my_type: integer, attention: string, target_close: date)

def randomized
  [0,1,2].sample
end

10.times do |count|
  Ticket.create(created: DateTime.now, owner: "David#{count}", priority: randomized,assigned: "Thomas#{count}", subject:"Cannot access reporting while using lorem ipsum#{count}", next_step: randomized, my_type: randomized, attention: "Steve#{count}",target_close: Date.today )
end

