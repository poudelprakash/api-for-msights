create first model

```
rails g model ticket created:datetime owner:string priority:integer assigned:string subject:string next_step:integer my_type:integer attention:string target_close:date
```

install active model serializers
```
gem "active_model_serializers", github: "rails-api/active_model_serializers", tag: "v0.10.0.rc5"
```

create initializer json_api.rb

scaffold resources

modify routes for serializers

create ember model
```
ember g model ticket created:date owner:string priority:integer assigned:string subject:string next_step:integer my_type:integer attention:string target_close:date
```

$E.store.findAll('ticket')
error:
ember.debug.js:32096 Error: Assertion Failed: Unable to find transform for 'integer'
    at new Error (native)
    at Error.EmberError (http://localhost:4200/assets/vendor.js:26684:21)
    at assert (http://localhost:4200/assets/vendor.js:16524:13)
    at Object.assert (http://localhost:4200/assets/vendor.js:26448:34)
    at assert (http://localhost:4200/assets/vendor.js:67906:37)
    at transformFor (http://localhost:4200/assets/vendor.js:82092:41)
    at http://localhost:4200/assets/vendor.js:80915:31
    at http://localhost:4200/assets/vendor.js:68988:18
    at cb (http://localhost:4200/assets/vendor.js:28491:11)
    at OrderedSet.forEach (http://localhost:4200/assets/vendor.js:28290:11)

soln: datatype should be changed from integer to number
```
ember g model ticket created:date owner:string priority:number assigned:string subject:string next_step:number my_type:number attention:string target_close:date
```

note: both date/datetime from seems to be handled by ember's date in same way

### make App accessible in ember inspecter
<http://emberigniter.com/accessing-global-object-in-ember-cli-app/#bonus-get-access-to-the-ember-data-store-from-the-app-object:8e48b1b8dc097d12b42c99999715cb13>
for our version of ember
```
// app/instance-initializers/global.js

export function initialize(application) {
  application.store = application.lookup('service:store');
  window.App = application;  // or window.Whatever
}

export default {
  name: 'global',
  initialize: initialize
};
```
